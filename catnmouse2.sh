#!/bin/bash
clear
DEFAULT_MAX_NUMBER=2048

THE_MAX_VALUE=${1:-$DEFAULT_MAX_NUMBER}

#echo "The max value is $THE_MAX_VALUE"

THE_NUMBER_IM_THINKING_OF=$((RANDOM%$THE_MAX_VALUE+1))

#echo "The number I'm thinking of is $THE_NUMBER_IM_THINKING_OF"

echo "OK cat, I'm thinking of a number from 1 to $THE_MAX_VALUE."
read -p "Make a guess: " GUESS
#echo "The number you guessed is $GUESS"

while [[(($GUESS -ne $THE_NUMBER_IM_THINKING_OF))]]
do
   if [[(($GUESS -lt 1))]];then
      #echo "$GUESS is less than 1"
      echo "No cat, You must enter a number that's >=1."
      read -p "Make a guess: " GUESS
      continue

   elif [[(($GUESS -lt $THE_NUMBER_IM_THINKING_OF))]];then
      #echo "$GUESS is lesser than $THE_NUMBRE_IM_THINKING_OF"
      echo "No cat, the number I'm thinking of a is larger than $GUESS."
      read -p "Make a guess: " GUESS
      continue

   elif [[(($GUESS -gt $THE_NUMBRE_IM_THINKING_OF))]];then

      if [[(($GUESS -gt $THE_MAX_VALUE))]];then
         #echo "$GUESS is greatest than (($THE_MAX_VALUE))"
         echo "No cat, You must enter a number that's <= $THE_MAX_VALUE."
         read -p "Make a guess: " GUESS
         continue

      else [[(($GUESS -lt $THE_NUMBER_IM_THINKING_OF))]]
         #echo "$GUESS is greater than $THE_NUMBRE_IM_THINKING_OF"
         echo "No cat, the number I'm thinking of is smaller than $GUESS."
         read -p "Make a guess: " GUESS
         continue
      fi
      continue

   else
      break

   fi

done

echo "You got me"

echo "                 _                                 "
echo "                 \\\`*-.                            "
echo "                  )  _\`-.                          "
echo "                  .  : \`. .                        "
echo "                  : _   '  \\                     "
echo "                  ; *\` _.   \`*-._                  "
echo "                  \`-.-'          \`-.               "
echo "                     ;       \`       \`.            "
echo "                      :.       .        \\         "
echo "                      . \\  .   :   .-'   .       "
echo "                      '  \`+.;  ;  '      :       "
echo "                      :  '  |    ;       ;-.      "
echo "    .b--.            ; '   : :\`-:     _.\`* ;      "
echo "    \`=,-,-'~~~      .*' /  .*\' ; .*\`- +'  \`*' "
echo "                   \`*-*   \`*-*  \`*-*\'             "

